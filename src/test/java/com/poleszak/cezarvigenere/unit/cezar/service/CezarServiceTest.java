package com.poleszak.cezarvigenere.unit.cezar.service;

import com.poleszak.cezarvigenere.cezar.model.request.EncodeCezarMessageRequest;
import com.poleszak.cezarvigenere.cezar.model.response.EncodeCezarMessageResponse;
import com.poleszak.cezarvigenere.cezar.service.CezarService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CezarServiceTest {

    @InjectMocks
    private CezarService cezarService;

    @Test
    public void testEncryptText() {
        // given
        String text = "hello world!";
        int key = 3;
        EncodeCezarMessageRequest request = new EncodeCezarMessageRequest(text, key);

        // when
        EncodeCezarMessageResponse response = cezarService.encryptText(request);

        // then
        assertEquals("khoor zruog!", response.encodedMessage());
        assertEquals(key, response.key());
    }

    @Test
    void testCrackCaesar() {
        String ciphertext = "L^dp wkh rqhI zkr lv wkh ehvwK";
        String expectedPlaintext = "c^am the one` who is the bestb";
        String plaintext = cezarService.crackCaesar(ciphertext);
        assertEquals(expectedPlaintext, plaintext);
    }

    @Test
    void testIsValidEnglish() {
        String validText = "the quick brown fox jumps over the lazy dog";
        assertTrue(cezarService.isValidEnglish(validText));

        String invalidText = "hsaghd fhdajfh lasidjg oaifhd";
        assertFalse(cezarService.isValidEnglish(invalidText));
    }

    @Test
    public void testEncryptTextWithNullText() {
        // given
        EncodeCezarMessageRequest request = new EncodeCezarMessageRequest(null, 3);

        // when, then
        assertThrows(IllegalStateException.class, () -> cezarService.encryptText(request));
    }

    @Test
    public void testEncryptTextWithNullKey() {
        // given
        EncodeCezarMessageRequest request = new EncodeCezarMessageRequest("hello world", null);

        // when, then
        assertThrows(IllegalStateException.class, () -> cezarService.encryptText(request));
    }
}

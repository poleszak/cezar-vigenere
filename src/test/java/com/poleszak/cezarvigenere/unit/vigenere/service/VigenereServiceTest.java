package com.poleszak.cezarvigenere.unit.vigenere.service;

import com.poleszak.cezarvigenere.vigenere.model.request.EncodeVigenereMessageRequest;
import com.poleszak.cezarvigenere.vigenere.model.response.EncodeVigenereMessageResponse;
import com.poleszak.cezarvigenere.vigenere.service.VigenereService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class VigenereServiceTest {

    @InjectMocks
    private VigenereService vigenereService;

    @Test
    public void testEncryptText() {
        EncodeVigenereMessageRequest request = new EncodeVigenereMessageRequest("hello", "key");
        EncodeVigenereMessageResponse response = vigenereService.encrypt(request);
        assertEquals("kbcol", response.encodedMessage());
        assertEquals("key", response.key());
    }

    @Test
    public void testEncryptTextWithNullInput() {
        EncodeVigenereMessageRequest request = new EncodeVigenereMessageRequest(null, "key");
        assertThrows(IllegalStateException.class, () -> {
            vigenereService.encrypt(request);
        });
    }

    @Test
    public void testEncryptTextWithNullKey() {
        EncodeVigenereMessageRequest request = new EncodeVigenereMessageRequest("hello", null);
        assertThrows(IllegalStateException.class, () -> {
            vigenereService.encrypt(request);
        });
    }
}
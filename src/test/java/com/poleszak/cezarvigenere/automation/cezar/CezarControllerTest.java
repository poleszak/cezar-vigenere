package com.poleszak.cezarvigenere.automation.cezar;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poleszak.cezarvigenere.cezar.controller.CezarController;
import com.poleszak.cezarvigenere.cezar.model.request.EncodeCezarMessageRequest;
import com.poleszak.cezarvigenere.cezar.model.response.EncodeCezarMessageResponse;
import com.poleszak.cezarvigenere.cezar.service.CezarService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CezarController.class)
public class CezarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CezarService cezarService;

    @Test
    public void testEncryptText() throws Exception {
        EncodeCezarMessageRequest request = new EncodeCezarMessageRequest("hello world!", 3);
        EncodeCezarMessageResponse response = new EncodeCezarMessageResponse("khoor#zruog$", 3);

        when(cezarService.encryptText(request)).thenReturn(response);

        // when
        mockMvc.perform(post("/api/v1/cezar/encrypt")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.encodedMessage").value("khoor#zruog$"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.key").value(3));
    }
}
package com.poleszak.cezarvigenere.automation.vigenere;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.poleszak.cezarvigenere.vigenere.controller.VigenereController;
import com.poleszak.cezarvigenere.vigenere.model.request.EncodeVigenereMessageRequest;
import com.poleszak.cezarvigenere.vigenere.model.response.EncodeVigenereMessageResponse;
import com.poleszak.cezarvigenere.vigenere.service.VigenereService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VigenereController.class)
public class VigenereControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private VigenereService vigenereService;

    @Test
    public void testEncryptText() throws Exception {
        EncodeVigenereMessageRequest request = new EncodeVigenereMessageRequest("hello", "key");
        EncodeVigenereMessageResponse response = new EncodeVigenereMessageResponse("uryyb", "key");

        when(vigenereService.encrypt(any(EncodeVigenereMessageRequest.class))).thenReturn(response);

        mockMvc.perform(post("/api/v1/vigenere/encrypt")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.encodedMessage").value("uryyb"))
                .andExpect(jsonPath("$.key").value("key"));
    }
}
package com.poleszak.cezarvigenere.cezar.service;

import com.poleszak.cezarvigenere.cezar.model.request.EncodeCezarMessageRequest;
import com.poleszak.cezarvigenere.cezar.model.response.EncodeCezarMessageResponse;
import org.springframework.stereotype.Service;

@Service
public class CezarService {

    public EncodeCezarMessageResponse encryptText(EncodeCezarMessageRequest request) {
        validateRequest(request);
        String text = request.text().toLowerCase();
        int key = request.key() % 26;

        StringBuilder result = new StringBuilder();
        for (char character : text.toCharArray()) {
            if (Character.isLetter(character)) {
                int originalAlphabetPosition = character - 'a';
                int newAlphabetPosition = (originalAlphabetPosition + key) % 26;
                char newCharacter = (char) ('a' + newAlphabetPosition);
                result.append(newCharacter);
            } else {
                result.append(character);
            }
        }
        return new EncodeCezarMessageResponse(result.toString(), key);
    }

    public String crackCaesar(String ciphertext) {
        StringBuilder plaintext = new StringBuilder();
        for (int shift = 0; shift < 26; shift++) {
            for (int i = 0; i < ciphertext.length(); i++) {
                char c = ciphertext.charAt(i);
                if (Character.isLetter(c)) {
                    char shifted = (char) (((c - 'a' + shift) % 26) + 'a');
                    plaintext.append(shifted);
                } else {
                    plaintext.append(c);
                }
            }
            if (isValidEnglish(plaintext.toString())) {
                return plaintext.toString();
            }
            plaintext = new StringBuilder();
        }
        return null;
    }

    public boolean isValidEnglish(String text) {
        String[] words = {"the", "and", "that", "with"};
        for (String word : words) {
            if (text.toLowerCase().contains(word)) {
                return true;
            }
        }
        return false;
    }

    private void validateRequest(EncodeCezarMessageRequest request) {
        if (request.text() == null || request.key() == null) {
            throw new IllegalStateException("Bad request body. Text or key can't be null!");
        }
    }
}

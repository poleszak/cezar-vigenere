package com.poleszak.cezarvigenere.cezar.controller;

import com.poleszak.cezarvigenere.cezar.model.request.EncodeCezarMessageRequest;
import com.poleszak.cezarvigenere.cezar.model.response.EncodeCezarMessageResponse;
import com.poleszak.cezarvigenere.cezar.service.CezarService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("api/v1/cezar")
@RequiredArgsConstructor
public class CezarController {

    private final CezarService cezarService;

    @PostMapping("/encrypt")
    public ResponseEntity<EncodeCezarMessageResponse> encrypt(@RequestBody EncodeCezarMessageRequest encodeMessageRequest) {
        return ResponseEntity.status(OK).body(cezarService.encryptText(encodeMessageRequest));
    }

    @PostMapping("/crack")
    public ResponseEntity<String> encrypt(@RequestBody String plaintext) {
        String decoded = cezarService.crackCaesar(plaintext);
        System.out.println(decoded);
        return ResponseEntity.status(OK).body(decoded);
    }
}

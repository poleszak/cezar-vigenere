package com.poleszak.cezarvigenere.cezar.model.request;

public record EncodeCezarMessageRequest(String text,
                                        Integer key) {
}

package com.poleszak.cezarvigenere.cezar.model.response;

public record EncodeCezarMessageResponse(String encodedMessage,
                                         Integer key) {
}

package com.poleszak.cezarvigenere.vigenere.controller;

import com.poleszak.cezarvigenere.vigenere.model.request.EncodeVigenereMessageRequest;
import com.poleszak.cezarvigenere.vigenere.model.response.EncodeVigenereMessageResponse;
import com.poleszak.cezarvigenere.vigenere.service.VigenereService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("api/v1/vigenere")
@RequiredArgsConstructor
public class VigenereController {

    private final VigenereService vigenereService;

    @PostMapping("/encrypt")
    public ResponseEntity<EncodeVigenereMessageResponse> encrypt(@RequestBody EncodeVigenereMessageRequest encodeVigenereMessageRequest) {
        return ResponseEntity.status(OK).body(vigenereService.encrypt(encodeVigenereMessageRequest));
    }
}

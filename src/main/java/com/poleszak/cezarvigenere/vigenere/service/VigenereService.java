package com.poleszak.cezarvigenere.vigenere.service;

import com.poleszak.cezarvigenere.vigenere.model.request.EncodeVigenereMessageRequest;
import com.poleszak.cezarvigenere.vigenere.model.response.EncodeVigenereMessageResponse;
import org.springframework.stereotype.Service;

@Service
public class VigenereService {
    public EncodeVigenereMessageResponse encrypt(EncodeVigenereMessageRequest request) {
        validateRequest(request);
        var text = request.text();
        var key = request.key();

        StringBuilder cipherText = new StringBuilder();
        int keyLength = key.length();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            int keyChar = key.charAt(i % keyLength) - 'a';
            char encryptedChar = (char) (((c + keyChar) % 26) + 'a');
            cipherText.append(encryptedChar);
        }

        return new EncodeVigenereMessageResponse(cipherText.toString(), key);
    }

    private void validateRequest(EncodeVigenereMessageRequest request) {
        if (request.text() == null || request.key() == null) {
            throw new IllegalStateException("Bad request body. Text or key can't be null!");
        }
    }
}

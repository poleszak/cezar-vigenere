package com.poleszak.cezarvigenere.vigenere.model.response;

public record EncodeVigenereMessageResponse(String encodedMessage,
                                         String key) {
}

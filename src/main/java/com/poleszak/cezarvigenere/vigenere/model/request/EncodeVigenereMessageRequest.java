package com.poleszak.cezarvigenere.vigenere.model.request;

public record EncodeVigenereMessageRequest(String text,
                                           String key) {
}

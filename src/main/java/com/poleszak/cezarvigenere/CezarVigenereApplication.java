package com.poleszak.cezarvigenere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CezarVigenereApplication {

	public static void main(String[] args) {
		SpringApplication.run(CezarVigenereApplication.class, args);
	}

}
